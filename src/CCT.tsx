import header from "./challengecode_header.txt?raw";

const CCT = () => {

    let output: HTMLTextAreaElement | undefined;

    const update = (event) => {
        if (!event.target) return;
        const target = event.target as HTMLTextAreaElement;
        let text = target.value;

        text = text.replace(/^[^<]*<hr>/, header);
        text = text.replace(/\[O\]/g, "[❌️]");
        text = text.replace(/\[.*_Title\]\(.*\)/g, "");
        text = text.replace(/\[.*\]\(https:\/\/anilist.co\/(anime|manga)\/([^/()]*\/?)\)/g, "https://anilist.co/$1/$2");

        text = text.replace("%START%", new Date().toISOString().split("T")[0]);

        if (output) {
            output.value = text;
        }
    };

    return (
        <div>
            <textarea
                id="input"
                rows={50}
                onInput={update}
                class="h-[99vh] w-[45%] mx-1 resize-none bg-dusk" />
            <textarea id="output"
                rows={50}
                ref={output}
                class="h-[99vh] w-[45%] mx-1 resize-none bg-dusk"
                readOnly />
        </div>
    );
};

export default CCT;