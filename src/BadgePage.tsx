import BadgeCategory from "./BadgeCategory";
import BadgeGroup from "./BadgeGroup";
import Badge from "./Badge";
import { Link } from "@solidjs/router";

const App = () =>
    <div>
        <h1>Anime</h1>
        <div class="flex flex-wrap">
            <BadgeCategory title="Monthly" revert>
                <BadgeGroup name="2020 December">
                    <Badge
                        name="Easy Mode"
                        image="https://i.postimg.cc/FRbRzDfP/Monthly-2020-December-Easy.gif"
                        url="https://anilist.co/forum/thread/20350/comment/530884"
                        started="2020-12-09"
                        completed="2020-12-31"
                        small />
                    <Badge
                        name="Hard Mode"
                        image="https://i.postimg.cc/KjZYMBBm/Monthly-2020-December-Hard.gif"
                        url="https://anilist.co/forum/thread/20350/comment/530884"
                        started="2020-12-09"
                        completed="2020-12-31" />
                </BadgeGroup>
                <BadgeGroup name="2021 January">
                    <Badge
                        name="Easy Mode"
                        image="https://i.postimg.cc/pTb1ZwzH/Monthly-2021-January-Easy-Animated.png"
                        url="https://anilist.co/forum/thread/21957/comment/573544"
                        started="2021-01-05"
                        completed="2021-01-30"
                        small />
                    <Badge
                        name="Hard Mode"
                        image="https://i.postimg.cc/XqxHnXhj/Monthly-2021-January-Hard-Animated.png"
                        url="https://anilist.co/forum/thread/21957/comment/573544"
                        started="2021-01-05"
                        completed="2021-01-30" />
                </BadgeGroup>
                <BadgeGroup name="2021 February">
                    <Badge
                        name="Easy Mode"
                        image="https://i.postimg.cc/NFXnMG87/Monthly-2021-Febraury-Easy-Animated.png"
                        url="https://anilist.co/forum/thread/23941/comment/627010"
                        started="2021-02-01"
                        completed="2021-02-18"
                        small />
                    <Badge
                        name="Hard Mode"
                        image="https://i.postimg.cc/05KFZZgM/Monthly-2021-Febraury-Hard-Animated.png"
                        url="https://anilist.co/forum/thread/23941/comment/627010"
                        started="2021-02-01"
                        completed="2021-02-18" />
                </BadgeGroup>
                <BadgeGroup name="2021 March">
                    <Badge
                        name="Easy Mode"
                        image="https://i.postimg.cc/2jnTVtRw/Monthly-2021-March-Easy-Animated.png"
                        url="https://anilist.co/forum/thread/26122/comment/697599"
                        started="2021-03-01"
                        completed="2021-03-18"
                        small />
                    <Badge
                        name="Hard Mode"
                        image="https://i.postimg.cc/gkDDK60L/Monthly-2021-March-Hard-Animated.png"
                        url="https://anilist.co/forum/thread/26122/comment/697599"
                        started="2021-03-01"
                        completed="2021-03-18" />
                </BadgeGroup>
                <BadgeGroup name="2021 April">
                    <Badge
                        name="Easy Mode"
                        image="https://i.postimg.cc/sxzYYVdJ/Monthly-2021-April-Easy-Animated.png"
                        url="https://anilist.co/forum/thread/29066/comment/788740"
                        started="2021-04-02"
                        completed="2021-04-30"
                        small />
                    <Badge
                        name="Hard Mode"
                        image="https://i.postimg.cc/kGyxwk7P/Monthly-2021-April-Hard-Animated.png"
                        url="https://anilist.co/forum/thread/29066/comment/788740"
                        started="2021-04-02"
                        completed="2021-04-30" />
                </BadgeGroup>
                <BadgeGroup name="2021 May">
                    <Badge
                        name="Easy Mode"
                        image="https://i.postimg.cc/VkhX79Pv/Monthly-2021-May-Easy-Animated.png"
                        url="https://anilist.co/forum/thread/32307/comment/893372"
                        started="2021-05-01"
                        completed="2021-05-29"
                        small />
                    <Badge
                        name="Hard Mode"
                        image="https://i.postimg.cc/6QGnF7B0/Monthly-2021-May-Hard-Animated.png"
                        url="https://anilist.co/forum/thread/32307/comment/893372"
                        started="2021-05-01"
                        completed="2021-05-29" />
                </BadgeGroup>
                <BadgeGroup name="2021 June">
                    <Badge
                        name="Easy Mode"
                        image="https://i.postimg.cc/jdNdsSCc/Monthly-2021-June-Easy-Animated.png"
                        url="https://anilist.co/forum/thread/35113/comment/988331"
                        started="2021-06-01"
                        completed="2021-06-27"
                        small />
                    <Badge
                        name="Hard Mode"
                        image="https://i.postimg.cc/zGrD3Ygr/Monthly-2021-June-Hard-Animated.png"
                        url="https://anilist.co/forum/thread/35113/comment/988331"
                        started="2021-06-01"
                        completed="2021-06-27" />
                </BadgeGroup>
                <BadgeGroup name="2021 July">
                    <Badge
                        name="Easy Mode"
                        image="https://i.postimg.cc/sfpsSLDL/Monthly-2021-July-Easy-Animated.png"
                        url="https://anilist.co/forum/thread/37307/comment/1064569"
                        started="2021-07-01"
                        completed="2021-07-31"
                        small />
                    <Badge
                        name="Hard Mode"
                        image="https://i.postimg.cc/zX85kkyG/Monthly-2021-July-Hard-Animated.png"
                        url="https://anilist.co/forum/thread/37307/comment/1064569"
                        started="2021-07-01"
                        completed="2021-07-31" />
                </BadgeGroup>
                <BadgeGroup name="2021 August">
                    <Badge
                        name="Easy Mode"
                        image="https://i.postimg.cc/C1JsBqTQ/Monthly-2021-August-Easy-Animated.png"
                        url="https://anilist.co/forum/thread/39659/comment/1149536"
                        started="2021-08-01"
                        completed="2021-08-28"
                        small />
                    <Badge
                        name="Hard Mode"
                        image="https://i.postimg.cc/pLD85FKJ/Monthly-2021-August-Hard-Animated.png"
                        url="https://anilist.co/forum/thread/39659/comment/1149536"
                        started="2021-08-01"
                        completed="2021-08-28" />
                </BadgeGroup>
                <BadgeGroup name="2021 September">
                    <Badge
                        name="Easy Mode"
                        image="https://i.postimg.cc/J4HHsCp1/Monthly-2021-September-Easy-Animated.png"
                        url="https://anilist.co/forum/thread/41792/comment/1221794"
                        started="2021-09-01"
                        completed="2021-09-30" />
                </BadgeGroup>
                <BadgeGroup name="2021 October">
                    <Badge
                        name="Easy Mode"
                        image="https://i.postimg.cc/QMPjYZnB/Monthly-2021-October-Easy-Animated.png"
                        url="https://anilist.co/forum/thread/43786/comment/1287277"
                        started="2021-10-01"
                        completed="2021-10-13"
                        small />
                    <Badge
                        name="Hard Mode"
                        image="https://i.postimg.cc/4N8qQb0y/Monthly-2021-October-Hard-Animated.png"
                        url="https://anilist.co/forum/thread/43786/comment/1287277"
                        started="2021-10-01"
                        completed="2021-10-13" />
                </BadgeGroup>
                <BadgeGroup name="2021 November">
                    <Badge
                        name="Easy Mode"
                        image="https://i.postimg.cc/xdy7KjVx/Monthly-2021-November-Easy-Animated.png"
                        url="https://anilist.co/forum/thread/45832/comment/1360769"
                        started="2021-11-01"
                        completed="2021-11-30"
                        small />
                    <Badge
                        name="Hard Mode"
                        image="https://i.postimg.cc/HnjhDQcR/Monthly-2021-November-Hard-Animated.png"
                        url="https://anilist.co/forum/thread/45832/comment/1360769"
                        started="2021-11-01"
                        completed="2021-11-30" />
                </BadgeGroup>
                <BadgeGroup name="2021 December">
                    <Badge
                        name="Easy Mode"
                        image="https://i.postimg.cc/DZbS3Zb1/Monthly-2021-December-Easy-Animated.png"
                        url="https://anilist.co/forum/thread/47411/comment/1418728"
                        started="2021-12-01"
                        completed="2021-12-23"
                        small />
                    <Badge
                        name="Hard Mode"
                        image="https://i.postimg.cc/PJWNNRyx/Monthly-2021-December-Hard-Animated.png"
                        url="https://anilist.co/forum/thread/47411/comment/1418728"
                        started="2021-12-01"
                        completed="2021-12-23" />
                </BadgeGroup>
                <BadgeGroup name="2022 January">
                    <Badge
                        name="Easy Mode"
                        image="https://i.postimg.cc/05LM2qqm/Monthly-2022-January-Easy-Animated.png"
                        url="https://anilist.co/forum/thread/48908/comment/1483030"
                        started="2022-01-01"
                        completed="2022-01-15"
                        small />
                    <Badge
                        name="Hard Mode"
                        image="https://i.postimg.cc/kgyBc6RF/Monthly-2022-January-Hard-Animated.png"
                        url="https://anilist.co/forum/thread/48908/comment/1483030"
                        started="2022-01-01"
                        completed="2022-01-15" />
                </BadgeGroup>
                <BadgeGroup name="2022 March">
                    <Badge
                        name="Easy Mode"
                        image="https://i.postimg.cc/gc63M2wM/Monthly-2022-March-Easy-Animated.png"
                        url="https://anilist.co/forum/thread/51651/comment/1600019"
                        started="2022-03-01"
                        completed="2022-03-29"
                        small />
                    <Badge
                        name="Hard Mode"
                        image="https://i.postimg.cc/L4znYYgM/Monthly-2022-March-Hard-Animated.png"
                        url="https://anilist.co/forum/thread/51651/comment/1600019"
                        started="2022-03-01"
                        completed="2022-03-29" />
                </BadgeGroup>
                <BadgeGroup name="2022 April">
                    <Badge
                        name="Easy Mode"
                        image="https://i.postimg.cc/PqmxtTWp/Monthly-2022-April-Easy-Animated.png"
                        url="https://anilist.co/forum/thread/52918/comment/1667383"
                        started="2022-04-01"
                        completed="2022-04-30"
                        small />
                    <Badge
                        name="Hard Mode"
                        image="https://i.postimg.cc/j5tj2mc8/Monthly-2022-April-Hard-Animated.png"
                        url="https://anilist.co/forum/thread/52918/comment/1667383"
                        started="2022-04-01"
                        completed="2022-04-30" />
                </BadgeGroup>
                <BadgeGroup name="2022 May">
                    <Badge
                        name="Easy Mode"
                        image="https://i.postimg.cc/7P2spDpR/Monthly-2022-May-Easy-Animated.png"
                        url="https://anilist.co/forum/thread/54234/comment/1729337"
                        started="2022-05-01"
                        completed="2022-05-11"
                        small />
                    <Badge
                        name="Hard Mode"
                        image="https://i.postimg.cc/3wN99HXn/Monthly-2022-May-Hard-Animated.png"
                        url="https://anilist.co/forum/thread/54234/comment/1729337"
                        started="2022-05-01"
                        completed="2022-05-11" />
                </BadgeGroup>
                <BadgeGroup name="2022 June">
                    <Badge
                        name="Easy Mode"
                        image="https://i.postimg.cc/vZcPTntr/Monthly-2022-June-Easy-Animated.png"
                        url="https://anilist.co/forum/thread/55490/comment/1788468"
                        started="2022-06-01"
                        completed="2022-06-26"
                        small />
                    <Badge
                        name="Hard Mode"
                        image="https://i.postimg.cc/t4BYS1KM/Monthly-2022-June-Hard-Animated.png"
                        url="https://anilist.co/forum/thread/55490/comment/1788468"
                        started="2022-06-01"
                        completed="2022-06-26" />
                </BadgeGroup>
                <BadgeGroup name="2022 July">
                    <Badge
                        name="Easy Mode"
                        image="https://i.postimg.cc/HkvbJPkn/Monthly-2022-July-Easy-Animated.png"
                        url="https://anilist.co/forum/thread/56620/comment/1843891"
                        started="2022-07-01"
                        completed="2022-07-10"
                        small />
                    <Badge
                        name="Hard Mode"
                        image="https://i.postimg.cc/qMzJJmSj/Monthly-2022-July-Hard-Animated.png"
                        url="https://anilist.co/forum/thread/56620/comment/1843891"
                        started="2022-07-01"
                        completed="2022-07-10" />
                </BadgeGroup>
                <BadgeGroup name="2023 April">
                    <Badge
                        name="Easy Mode"
                        image="https://i.postimg.cc/mZ9C2y8Q/Monthly-2023-April-Easy-Static.png"
                        url="https://anilist.co/forum/thread/64140/comment/2222598"
                        started="2023-04-01"
                        small />
                    <Badge
                        name="Hard Mode"
                        image="https://i.postimg.cc/Rh1W1GDy/Monthly-2023-April-Hard-Static.png"
                        url="https://anilist.co/forum/thread/64140/comment/2222598"
                        started="2023-04-01" />
                </BadgeGroup>
            </BadgeCategory>
            <BadgeCategory title="Seasonal" revert>
                <BadgeGroup name="2020" revert>
                    <Badge
                        name="Fall 2020"
                        image="https://i.postimg.cc/66LhwpQ3/2020-Seasonal-Fall.gif"
                        url="https://anilist.co/forum/thread/17645/comment/573732"
                        started="2021-01-05"
                        completed="2021-01-15" />
                </BadgeGroup>
                <BadgeGroup name="2021" revert>
                    <Badge
                        name="Winter 2021"
                        image="https://i.postimg.cc/ZKc2rKhj/Seasonal-2021-Winter-Animated.png"
                        url="https://anilist.co/forum/thread/22075/comment/573633"
                        started="2021-01-05"
                        completed="2021-03-30" />
                    <Badge
                        name="Spring 2021"
                        image="https://i.postimg.cc/qMF7JT8B/Seasonal-2021-Spring-Animated.png"
                        url="https://anilist.co/forum/thread/29268/comment/795318"
                        started="2021-04-04"
                        completed="2021-07-06" />
                    <Badge
                        name="Summer 2021"
                        image="https://i.postimg.cc/c4pj9r8m/Seasonal-2021-Summer-Animated.png"
                        url="https://anilist.co/forum/thread/37560/comment/1074936"
                        started="2021-07-04"
                        completed="2021-10-03" />
                    <Badge
                        name="Fall 2021"
                        image="https://i.postimg.cc/rwTJkstS/Seasonal-2021-Fall-Animated.png"
                        url="https://anilist.co/forum/thread/43932/comment/1292687"
                        started="2021-10-03"
                        completed="2022-01-08" />
                </BadgeGroup>
                <BadgeGroup name="2022" revert>
                    <Badge
                        name="Winter 2022"
                        image="https://i.postimg.cc/bwXpFWG5/Seasonal-2022-Winter-Animated.png"
                        url="https://anilist.co/forum/thread/48962/comment/1483789"
                        started="2022-01-02"
                        completed="2022-04-01" />
                    <Badge
                        name="Spring 2022"
                        image="https://i.postimg.cc/JhZTLbVr/Seasonal-2022-Spring-Animated.png"
                        url="https://anilist.co/forum/thread/52999/comment/1671080"
                        started="2022-04-03"
                        completed="2022-07-10" />
                    <Badge
                        name="Summer 2022"
                        image="https://i.postimg.cc/CKsp16LY/Seasonal-2022-Summer-Animated.png"
                        url="https://anilist.co/forum/thread/56687/comment/1846973"
                        started="2022-07-03"
                        completed="2022-10-16" />
                </BadgeGroup>
                <BadgeGroup name="2023" revert>
                    <Badge
                        name="Winter 2023"
                        url="https://anilist.co/forum/thread/61848/comment/2095453"
                        started="2023-01-02"
                        completed="2023-04-03" />
                    <Badge
                        name="Spring 2023"
                        url="https://anilist.co/forum/thread/64247/comment/2249552"
                        started="2023-04-23" />
                </BadgeGroup>
            </BadgeCategory>
            <BadgeCategory title="Yearly" revert>
                <BadgeGroup name="2014">
                    <Badge
                        name="2014 Classic"
                        image="https://i.postimg.cc/bdXLhptg/2014-Classic.png"
                        url="https://anilist.co/forum/thread/59780/comment/2005271"
                        started="2022-10-07"
                    />
                </BadgeGroup>
                <BadgeGroup name="2015">
                    <Badge
                        name="2015 Classic"
                        image="https://i.postimg.cc/KcMpp7f0/2015-Classic.png"
                        url="https://anilist.co/forum/thread/57229/comment/1866745"
                        started="2022-07-16"
                    />
                </BadgeGroup>
                <BadgeGroup name="2016">
                    <Badge
                        name="2016 Classic"
                        image="https://i.postimg.cc/sfZrGbfy/2016-Classic.png"
                        url="https://anilist.co/forum/thread/39961/comment/1162173"
                        started="2021-08-06" />
                </BadgeGroup>
                <BadgeGroup name="2017">
                    <Badge
                        name="2017 Classic"
                        image="https://i.postimg.cc/pLDXZPP9/2017-Classic.png"
                        url="https://anilist.co/forum/thread/13347/comment/672056"
                        started="2021-02-19"
                        completed="2021-02-19" />
                </BadgeGroup>
                <BadgeGroup name="2018">
                    <Badge
                        name="2018 Classic"
                        image="https://i.postimg.cc/50Jz3PcJ/2018-Classic.png"
                        url="https://anilist.co/forum/thread/7104/comment/672029"
                        started="2021-02-19"
                        completed="2021-03-04" />
                </BadgeGroup>
                <BadgeGroup name="2019">
                    <Badge
                        name="2019 Classic"
                        image="https://i.postimg.cc/nc9LXv5G/2019-Classic.png"
                        url="https://anilist.co/forum/thread/10377/comment/671974"
                        started="2021-02-19"
                        completed="2021-02-19" />
                </BadgeGroup>
                <BadgeGroup name="2020">
                    <Badge
                        name="Staff Picks 2020"
                        image="https://i.postimg.cc/rpfJ68Cb/2020-Staff-Picks-Animated.png"
                        url="https://anilist.co/forum/thread/21631/comment/573693"
                        started="2021-01-05"
                        completed="2021-12-10" />
                    <Badge
                        name="2020 Classic"
                        image="https://i.postimg.cc/nrmc88KR/2020-Classic.png"
                        url="https://anilist.co/forum/thread/22395/comment/673016"
                        started="2021-02-19"
                        completed="2022-04-02" />
                </BadgeGroup>
                <BadgeGroup name="2021">
                    <Badge
                        name="Staff Picks 2021"
                        image="https://i.postimg.cc/rFW1nR6P/2021-Staff-Picks-Animated.png"
                        url="https://anilist.co/forum/thread/48560/comment/1471478"
                        started="2021-12-25"
                        completed="2022-12-24" />
                    <Badge
                        name="2021 Classic"
                        image="https://i.postimg.cc/Jz9KSdx9/2021-Classic.png"
                        url="https://anilist.co/forum/thread/49205/comment/1491298"
                        started="2022-01-07" />
                </BadgeGroup>
                <BadgeGroup name="2022">
                    <Badge
                        name="2022 Classic"
                        image="https://i.postimg.cc/900XLzsy/2022-Classic.png"
                        url="https://anilist.co/forum/thread/61944/comment/2108726"
                        started="2022-01-07" />
                </BadgeGroup>
            </BadgeCategory>
            <BadgeCategory title="Tier">
                <Badge
                    name="Beginner's"
                    image="https://i.postimg.cc/VLyPvk56/Tier-Anime-Beginners.png"
                    url="https://anilist.co/forum/thread/4448/comment/671691"
                    started="2021-02-18"
                    completed="2021-04-03" />
                <Badge
                    name="Intermediate"
                    image="https://i.postimg.cc/L5mT1nTx/Tier-Anime-Intermediate.png"
                    url="https://anilist.co/forum/thread/5027/comment/806537"
                    started="2021-04-07"
                    completed="2021-10-10" />
                <Badge
                    name="Advanced"
                    image="https://i.postimg.cc/DZvJQs94/Tier-Anime-Advanced.png"
                    url="https://anilist.co/forum/thread/8462/comment/1309828"
                    started="2021-10-10" />
            </BadgeCategory>
            <BadgeCategory title="Special">
                <Badge // Release: 2018-12-28
                    name="Gambler's 1.1"
                    image="https://i.postimg.cc/44jTkGWm/Special-Gamblers.png"
                    url="https://anilist.co/forum/thread/6171/comment/1272241"
                    started="2021-09-24" />
                <Badge // Release: 2019-02-05
                    name="Chinese Zodiac"
                    image="https://i.postimg.cc/dVjMyywy/Special-Zodiac.png"
                    url="https://anilist.co/forum/thread/6553/comment/670948"
                    started="2021-02-18" />
                <Badge // Release: 2019-06-01
                    name="Rainbow"
                    image="https://i.postimg.cc/nhJbrkbx/Special-Rainbow.png"
                    url="https://anilist.co/forum/thread/7738/comment/671696"
                    started="2021-02-18"
                    completed="2021-03-20" />
                <Badge // Release: 2019-09-13
                    name="Archaeologist's"
                    image="https://i.postimg.cc/c4YGdzzT/Special-Archaeologists.png"
                    url="https://anilist.co/forum/thread/9282/comment/671704"
                    started="2021-02-18" />
                <Badge // Release: 2019-10-25
                    name="Monster"
                    image="https://i.postimg.cc/9XDjn0t6/Special-Monster.png"
                    url="https://anilist.co/forum/thread/9777/comment/671709"
                    started="2021-02-18"
                    completed="2021-07-11" />
                <Badge // Release: 2020-07-10
                    name="Recommendations"
                    image="https://i.postimg.cc/rFCcSb9s/Special-Recommendations.png"
                    url="https://anilist.co/forum/thread/14236/comment/741395"
                    started="2021-03-16" />
                <Badge // Release: 2021-01-15
                    name="Quiz"
                    image="https://i.postimg.cc/RCWCL4QV/Special-AWC-Quiz.png"
                    url="https://anilist.co/forum/thread/22800/comment/671790"
                    started="2021-02-18"
                    completed="2021-06-22" />
                <Badge // Release: 2021-03-12
                    name="Marathon"
                    image="https://i.postimg.cc/zvscnnwW/Special-Marathon.png"
                    url="https://anilist.co/forum/thread/27098/comment/730251"
                    started="2021-03-12" />
                <Badge // Release: 2021-03-26
                    name="Gambler's 2.0"
                    image="https://i.postimg.cc/kG4KWhrZ/Special-Gamblers-2-0.png"
                    url="https://anilist.co/forum/thread/28272/comment/766004"
                    started="2021-03-26" />
                <Badge // Release: 2021-04-01
                    name="Masterpiece"
                    image="https://i.postimg.cc/cLTpbTQH/Collection-Anthology-Masterpiece.png"
                    url="https://anilist.co/forum/thread/28916/comment/785324"
                    started="2021-04-01" />
                <Badge // Release: 2021-07-23
                    name="Olympic Games"
                    image="https://i.postimg.cc/zfbKHqdP/Special-Olympic-Games.png"
                    url="https://anilist.co/forum/thread/39077/comment/1127525"
                    started="2021-07-23" />
                <Badge // Release: 2021-08-20
                    name="Western Zodiac"
                    image="https://i.postimg.cc/RZKLfNS2/Special-Zodiac-II.png"
                    url="https://anilist.co/forum/thread/40936/comment/1196389"
                    started="2021-08-21"
                    completed="2022-03-27" />
                <Badge // Release: 2021-10-29
                    name="Hero’s Dawn"
                    image="https://i.postimg.cc/pT7LnL51/Special-Heros-Dawn.png"
                    url="https://anilist.co/forum/thread/45664/comment/1355672"
                    started="2021-10-29"
                    completed="2022-07-15" />
                <Badge // Release: 2022-02-18
                    name="Pioneers' Classic"
                    image="https://i.postimg.cc/JntTv8Bj/Pioneers-Classic.png"
                    url="https://anilist.co/forum/thread/51196/comment/1580819"
                    started="2022-02-18" />
                {/*
                <Badge // Release: 2022-07-25
                    name="Hero’s War"
                    image="https://i.postimg.cc/50ShvP0G/Hero-s-War-Generic.png"
                    url="https://anilist.co/forum/thread/57577/comment/1888681"
                    started="2022-07-28" />
                */}
                <Badge // Release: 2022-10-14
                    name="Community Workshop"
                    image="https://i.postimg.cc/wvYPFvm2/Special-Community-Workshop.png"
                    url="https://anilist.co/forum/thread/59986/comment/2014167"
                    started="2022-10-14" />
                <Badge
                    name="Retro Classic"
                    image="https://i.postimg.cc/fLYmb36S/Retro-Classic.png"
                    url="https://anilist.co/forum/thread/63239/comment/2207419"
                    started="2023-03-17" />
                <Badge
                    name="Hidden Gems"
                    image="https://i.postimg.cc/XqCZ2k48/Special-Hidden-Gems.png"
                    url="https://anilist.co/forum/thread/63608/comment/2207425"
                    started="2023-03-17" />
            </BadgeCategory>
            <BadgeCategory title="Franchise">
                <Badge // Release: 2019-05-17
                    name="Monogatari"
                    image="https://i.postimg.cc/h4zMZwPg/Collection-Franchise-Monogatari.png"
                    url="https://anilist.co/forum/thread/7566/comment/637676"
                    started="2021-02-05"
                    completed="2021-02-05" />
                <Badge // Release: 2019-06-21
                    name="JoJo's Bizarre Adventure"
                    image="https://i.postimg.cc/g0tqNyCn/Collection-Franchise-Jojo-s.png"
                    url="https://anilist.co/forum/thread/8037/comment/671895"
                    started="2021-02-18"
                    completed="2021-02-21" />
                <Badge // Release: 2020-05-29
                    name="Initial D"
                    image="https://i.postimg.cc/YqBNnCsf/Collection-Franchise-Initial-D.png"
                    url="https://anilist.co/forum/thread/12963/comment/671899"
                    started="2021-02-18" />
                <Badge // Release: 2020-10-09
                    name="Higurashi"
                    image="https://i.postimg.cc/k4t8s0Np/Collection-Franchise-Higurashi.png"
                    url="https://anilist.co/forum/thread/17846/comment/671903"
                    started="2021-02-18" />
                <Badge // Release: 2021-02-19
                    name="Gintama"
                    image="https://i.postimg.cc/025c4JVP/Collection-Franchise-Gintama.png"
                    url="https://anilist.co/forum/thread/25322/comment/672075"
                    started="2021-02-19" />
                <Badge // Release: 2021-02-26
                    name="Love Live!"
                    image="https://i.postimg.cc/85KKtL7n/Collection-Franchise-Love-Live.png"
                    url="https://anilist.co/forum/thread/25914/comment/691309"
                    started="2021-02-26" />
                <Badge // Release: 2021-08-13
                    name="Evangelion"
                    image="https://i.postimg.cc/0QSTJVPj/Collection-Franchise-Evangelion.png"
                    url="https://anilist.co/forum/thread/40441/comment/1178983"
                    started="2021-08-13" />
                <Badge // Release: 2021-10-15
                    name="Cardcaptor Sakura"
                    image="https://i.postimg.cc/nzBPrJYT/Collection-Franchise-Cardcaptor-Sakura.png"
                    url="https://anilist.co/forum/thread/44858/comment/1326181"
                    started="2021-10-15" />
                <Badge // Release: 2021-12-10
                    name="Aria"
                    image="https://i.postimg.cc/qvYG9PYQ/Collection-Franchise-Aria.png"
                    url="https://anilist.co/forum/thread/47806/comment/1442094"
                    started="2021-12-10" />
                <Badge // Release: 2022-02-11
                    name="Japanese Animated Film Classics"
                    image="https://i.postimg.cc/MTmbWVLG/Collection-Anthology-JAFC.png"
                    url="https://anilist.co/forum/thread/50869/comment/1563542"
                    started="2022-02-11" />
                <Badge // Release: 2022-04-15
                    name="Urusei Yatsura"
                    image="https://i.postimg.cc/JzY1TN8x/Collection-Franchise-Urusei-Yatsura.png"
                    url="https://anilist.co/forum/thread/53566/comment/1697108"
                    started="2022-04-15" />
                <Badge // Release: 2022-08-12
                    name="Pretty Cure"
                    image="https://i.postimg.cc/jqgsdTPW/Collection-Franchise-Pretty-Cure.png"
                    url="https://anilist.co/forum/thread/58215/comment/1930731"
                    started="2022-08-18" />
                <Badge // Release: 2022-09-16
                    name="Inuyasha"
                    image="https://i.postimg.cc/fTnZrPX2/Collection-Franchise-Inuyasha.png"
                    url="https://anilist.co/forum/thread/59212/comment/1969534"
                    started="2022-09-16" />
            </BadgeCategory>
            <BadgeCategory title="Studio">
                <BadgeGroup name="Studio Ghibli"> {/* Release: 2019-04-26 */}
                    <Badge
                        name="Bingo!"
                        image="https://i.postimg.cc/W4L2Bx0Z/Collection-Studio-Studio-Ghibli-Line.png"
                        url="https://anilist.co/forum/thread/7284/comment/671732"
                        started="2021-02-18" />
                    <Badge
                        name="Clear the Board!"
                        image="https://i.postimg.cc/fbcwD5vr/Collection-Studio-Studio-Ghibli-Board.png"
                        url="https://anilist.co/forum/thread/7284/comment/671732"
                        small />
                </BadgeGroup>
                <BadgeGroup name="Kyoto Animation"> {/* Release: 2019-09-20 */}
                    <Badge
                        name="Bingo!"
                        image="https://i.postimg.cc/JnRRnJ64/Collection-Studio-Kyoto-Animation-Line.png"
                        url="https://anilist.co/forum/thread/9352/comment/671765"
                        started="2021-02-18" />
                    <Badge
                        name="Clear the Board!"
                        image="https://i.postimg.cc/gJ4m6JQy/Collection-Studio-Kyoto-Animation-Board.png"
                        url="https://anilist.co/forum/thread/9352/comment/671765"
                        small />
                </BadgeGroup>
                <BadgeGroup name="Wit Studio"> {/* Release: 2020-06-26 */}
                    <Badge
                        name="Bingo!"
                        image="https://i.postimg.cc/RhF91mxk/Collection-Studio-Wit-Studio-Line.png"
                        url="https://anilist.co/forum/thread/13775/comment/671810"
                        started="2021-02-18"
                        completed="2021-02-18" />
                    <Badge
                        name="Clear the Board!"
                        image="https://i.postimg.cc/NGJQsK1B/Collection-Studio-Wit-Studio-Board.png"
                        url="https://anilist.co/forum/thread/13775/comment/671810"
                        started="2021-02-18"
                        small />
                </BadgeGroup>
                <BadgeGroup name="Madhouse"> {/* Release: 2021-06-18 */}
                    <Badge
                        name="Bingo!"
                        image="https://i.postimg.cc/wjhtDxxj/Collection-Studio-Madhouse-Line.png"
                        url="https://anilist.co/forum/thread/36321/comment/1033548"
                        started="2021-06-18" />
                    <Badge
                        name="Clear the Board!"
                        image="https://i.postimg.cc/wBg12K3c/Collection-Studio-Madhouse-Board.png"
                        url="https://anilist.co/forum/thread/36321/comment/1033548"
                        small />
                </BadgeGroup>
            </BadgeCategory>
            <BadgeCategory title="Legend">
                <Badge // Release: 2019-05-17
                    name="Satoshi Kon"
                    image="https://i.postimg.cc/YqcmCcZ6/Collection-Legend-Satoshi-Kon.png"
                    url="https://anilist.co/forum/thread/7567/comment/671910"
                    started="2021-02-19" />
                <Badge // Release: 2019-08-23
                    name="Makoto Shinkai"
                    image="https://i.postimg.cc/3JTFww7f/Collection-Legend-Makoto-Shinkai.png"
                    url="https://anilist.co/forum/thread/9002/comment/671924"
                    started="2021-02-19"
                    completed="2022-03-26" />
                <Badge // Release: 2020-08-28
                    name="Isao Takahata"
                    image="https://i.postimg.cc/HLVQNc4n/Collection-Legend-Isao-Takahata.png"
                    url="https://anilist.co/forum/thread/16264/comment/671933"
                    started="2021-02-19" />
                <Badge // Release: 2020-11-20
                    name="Kunihiko Ikuhara"
                    image="https://i.postimg.cc/dtjmZKVs/Collection-Legend-Kunihiko-Ikuhara.png"
                    url="https://anilist.co/forum/thread/19773/comment/671940"
                    started="2021-02-19" />
                <Badge // Release: 2022-08-19
                    name="Mitsuru Adachi"
                    image="https://i.postimg.cc/J4B5wFTH/Collection-Legend-Mitsuru-Adachi.png"
                    url="https://anilist.co/forum/thread/58423/comment/1932110"
                    started="2022-08-19" />
            </BadgeCategory>
            <BadgeCategory title="Puzzle">
                <Badge // Release: 2020-03-21
                    name="Genre Jigsaw"
                    image="https://i.postimg.cc/zBsysW-sB/Puzzle-Genre-Jigsaw.png"
                    url="https://anilist.co/forum/thread/11405/comment/673089"
                    started="2021-02-19"
                    completed="2021-12-23" />
                <Badge // Release: 2020-06-19
                    name="Double Dipping"
                    image="https://i.postimg.cc/90Hf829w/Puzzle-Double-Dip.png"
                    url="https://anilist.co/forum/thread/13560/comment/673105"
                    started="2021-02-19"
                    completed="2021-11-29" />
                <Badge // Release: 2020-09-18
                    name="Tag Domino"
                    image="https://i.postimg.cc/vBfY6KKY/Puzzle-Tag-Domino.png"
                    url="https://anilist.co/forum/thread/17048/comment/673118"
                    started="2021-02-19"
                    completed="2021-08-21" />
                <Badge // Release: 2021-04-16
                    name="Battleship"
                    image="https://i.postimg.cc/brPz0TXQ/Puzzle-Battleship.png"
                    url="https://anilist.co/forum/thread/30744/comment/842854"
                    started="2021-04-16"
                    completed="2021-07-31" />
                <Badge // Release: 2021-11-19
                    name="Gomoku"
                    image="https://i.postimg.cc/ht5mtNgx/Puzzle-Gumoku.png"
                    url="https://anilist.co/forum/thread/46745/comment/1394879"
                    started="2021-11-19"
                    completed="2022-04-30" />
                <Badge // Release: 2022-03-11
                    name="Plus-Minus"
                    image="https://i.postimg.cc/HWQL2wXQ/Puzzle-Plus-Minus.png"
                    url="https://anilist.co/forum/thread/52011/comment/1619521"
                    started="2022-03-11" />
                <Badge // Release: 2022-07-08
                    name="Tetris"
                    image="https://i.postimg.cc/QtLpPVZC/Puzzle-Tetris.png"
                    url="https://anilist.co/forum/thread/56903/comment/1853820"
                    started="2022-07-08" />
            </BadgeCategory>
            <BadgeCategory title="Genre">
                <BadgeGroup name="Action">
                    <Badge
                        name="Easy Mode"
                        image="https://i.postimg.cc/fLGrjzCB/Genre-Action-Easy.png"
                        url="https://anilist.co/forum/thread/5288/comment/675366"
                        started="2021-02-20"
                        completed="2021-07-13"
                        small />
                    <Badge
                        name="Normal Mode"
                        image="https://i.postimg.cc/RVzYW0N0/Genre-Action-Normal.png"
                        url="https://anilist.co/forum/thread/5288/comment/675366"
                        started="2021-02-20"
                        completed="2022-04-17" />
                    <Badge
                        name="Hard Mode"
                        image="https://i.postimg.cc/kXFpKdbv/Genre-Action-Hard.png"
                        url="https://anilist.co/forum/thread/5288/comment/675366"
                        started="2021-02-20"
                        small />
                </BadgeGroup>
                <BadgeGroup name="Adventure">
                    <Badge
                        name="Easy Mode"
                        image="https://i.postimg.cc/xTR6qyws/Genre-Adventure-Easy.png"
                        url="https://anilist.co/forum/thread/6111/comment/675370"
                        started="2021-02-20"
                        completed="2021-12-16" />
                    <Badge
                        name="Normal Mode"
                        image="https://i.postimg.cc/wBSVnWNB/Genre-Adventure-Normal.png"
                        url="https://anilist.co/forum/thread/6111/comment/675370"
                        started="2021-02-20"
                        small />
                    <Badge
                        name="Hard Mode"
                        image="https://i.postimg.cc/NFtxj5pq/Genre-Adventure-Hard.png"
                        url="https://anilist.co/forum/thread/6111/comment/675370"
                        small />
                </BadgeGroup>
                <BadgeGroup name="Comedy">
                    <Badge
                        name="Easy Mode"
                        image="https://i.postimg.cc/NFxk9Bzy/Genre-Comedy-Easy.png"
                        url="https://anilist.co/forum/thread/5289/comment/675371"
                        started="2021-02-20"
                        completed="2021-06-20"
                        small />
                    <Badge
                        name="Normal Mode"
                        image="https://i.postimg.cc/GhcQ0T4v/Genre-Comedy-Normal.png"
                        url="https://anilist.co/forum/thread/5289/comment/675371"
                        started="2021-02-20"
                        completed="2021-11-13"
                        small />
                    <Badge
                        name="Hard Mode"
                        image="https://i.postimg.cc/4NB1X4cq/Genre-Comedy-Hard.png"
                        url="https://anilist.co/forum/thread/5289/comment/675371"
                        started="2021-02-20"
                        completed="2022-07-10" />
                    <Badge
                        name="Mastery"
                        image="https://i.postimg.cc/85wp9w7p/AWC-Genre-Comedy-Mastery-Animated.png"
                        url="https://anilist.co/forum/thread/5289/comment/1864875"
                        started="2022-07-15"
                        small />
                </BadgeGroup>
                <BadgeGroup name="Drama">
                    <Badge
                        name="Easy Mode"
                        image="https://i.postimg.cc/bvbbSqGz/Genre-Drama-Easy.png"
                        url="https://anilist.co/forum/thread/5875/comment/675376"
                        started="2021-02-20"
                        completed="2021-04-07"
                        small />
                    <Badge
                        name="Normal Mode"
                        image="https://i.postimg.cc/Qt37pv5c/Genre-Drama-Normal.png"
                        url="https://anilist.co/forum/thread/5875/comment/675376"
                        started="2021-02-20"
                        completed="2021-07-16"
                        small />
                    <Badge
                        name="Hard Mode"
                        image="https://i.postimg.cc/4y8hRd01/Genre-Drama-Hard.png"
                        url="https://anilist.co/forum/thread/5875/comment/675376"
                        started="2021-02-20"
                        completed="2021-10-14" />
                    <Badge
                        name="Mastery"
                        image="https://i.postimg.cc/cHRcSrzQ/AWC-Genre-Drama-Mastery-Animated.png"
                        url="https://anilist.co/forum/thread/5875/comment/1369148"
                        started="2021-11-05"
                        small />
                </BadgeGroup>
                <BadgeGroup name="Ecchi">
                    <Badge
                        name="Easy Mode"
                        image="https://i.postimg.cc/HW3MyZL7/Genre-Ecchi-Easy.png"
                        url="https://anilist.co/forum/thread/6631/comment/675377"
                        started="2021-02-20"
                        completed="2022-05-05" />
                    <Badge
                        name="Normal Mode"
                        image="https://i.postimg.cc/yYdZXmJp/Genre-Ecchi-Normal.png"
                        url="https://anilist.co/forum/thread/6631/comment/675377"
                        started="2021-02-20"
                        small />
                    <Badge
                        name="Hard Mode"
                        image="https://i.postimg.cc/QxyW6F18/Genre-Ecchi-Hard.png"
                        url="https://anilist.co/forum/thread/6631/comment/675377"
                        small />
                </BadgeGroup>
                <BadgeGroup name="Fantasy">
                    <Badge
                        name="Easy Mode"
                        image="https://i.postimg.cc/YCLLTtz1/Genre-Fantasy-Easy.png"
                        url="https://anilist.co/forum/thread/5290/comment/675380"
                        started="2021-02-20"
                        completed="2021-09-26" />
                    <Badge
                        name="Normal Mode"
                        image="https://i.postimg.cc/5tcQVDsD/Genre-Fantasy-Normal.png"
                        url="https://anilist.co/forum/thread/5290/comment/675380"
                        started="2021-02-20"
                        completed="2022-10-16"
                        small />
                    <Badge
                        name="Hard Mode"
                        image="https://i.postimg.cc/YS1Gq8V0/Genre-Fantasy-Hard.png"
                        url="https://anilist.co/forum/thread/5290/comment/675380"
                        started="2021-02-20"
                        small />
                </BadgeGroup>
                <BadgeGroup name="Hentai">
                    <Badge
                        name="Easy Mode"
                        image="https://i.postimg.cc/0241bvRX/Genre-Hentai-Easy.png"
                        url="https://anilist.co/forum/thread/5559/comment/722221"
                        started="2021-03-09"
                        completed="2021-07-24" />
                    <Badge
                        name="Normal Mode"
                        image="https://i.postimg.cc/Pr90dMYB/Genre-Hentai-Normal.png"
                        url="https://anilist.co/forum/thread/5559/comment/722221"
                        started="2021-03-09"
                        small />
                    <Badge
                        name="Hard Mode"
                        image="https://i.postimg.cc/52zTyFmj/Genre-Hentai-Hard.png"
                        url="https://anilist.co/forum/thread/5559/comment/722221"
                        small />
                </BadgeGroup>
                <BadgeGroup name="Horror">
                    <Badge
                        name="Easy Mode"
                        image="https://i.postimg.cc/d0KXytW6/Genre-Horror-Easy.png"
                        url="https://anilist.co/forum/thread/5558/comment/675381"
                        started="2021-02-20"
                        completed="2022-05-11" />
                    <Badge
                        name="Normal Mode"
                        image="https://i.postimg.cc/9MmnLgHq/Genre-Horror-Normal.png"
                        url="https://anilist.co/forum/thread/5558/comment/675381"
                        started="2021-02-20"
                        small />
                    <Badge
                        name="Hard Mode"
                        image="https://i.postimg.cc/j5Mkm6R3/Genre-Horror-Hard.png"
                        url="https://anilist.co/forum/thread/5558/comment/675381"
                        small />
                </BadgeGroup>
                <BadgeGroup name="Mahou Shoujo">
                    <Badge
                        name="Easy Mode"
                        image="https://i.postimg.cc/0jrHs6Qk/Genre-Mahou-Shoujo-Easy.png"
                        url="https://anilist.co/forum/thread/6376/comment/675383"
                        started="2021-02-20" />
                    <Badge
                        name="Normal Mode"
                        image="https://i.postimg.cc/sx8HDVQp/Genre-Mahou-Shoujo-Normal.png"
                        url="https://anilist.co/forum/thread/6376/comment/675383"
                        small />
                    <Badge
                        name="Hard Mode"
                        image="https://i.postimg.cc/2jXKxxcV/Genre-Mahou-Shoujo-Hard.png"
                        url="https://anilist.co/forum/thread/6376/comment/675383"
                        small />
                </BadgeGroup>
                <BadgeGroup name="Mecha">
                    <Badge
                        name="Easy Mode"
                        image="https://i.postimg.cc/vHJ2JZdy/Genre-Mecha-Easy.png"
                        url="https://anilist.co/forum/thread/6112/comment/675387"
                        started="2021-02-20" />
                    <Badge
                        name="Normal Mode"
                        image="https://i.postimg.cc/RZwp4xMH/Genre-Mecha-Normal.png"
                        url="https://anilist.co/forum/thread/6112/comment/675387"
                        small />
                    <Badge
                        name="Hard Mode"
                        image="https://i.postimg.cc/q70b7DRZ/Genre-Mecha-Hard.png"
                        url="https://anilist.co/forum/thread/6112/comment/675387"
                        small />
                </BadgeGroup>
                <BadgeGroup name="Music">
                    <Badge
                        name="Easy Mode"
                        image="https://i.postimg.cc/hjt2LJVL/Genre-Music-Easy.png"
                        url="https://anilist.co/forum/thread/6375/comment/675391"
                        started="2021-02-20"
                        completed="2022-01-09" />
                    <Badge
                        name="Normal Mode"
                        image="https://i.postimg.cc/DZpBB2ct/Genre-Music-Normal.png"
                        url="https://anilist.co/forum/thread/6375/comment/675391"
                        started="2021-02-20"
                        small />
                    <Badge
                        name="Hard Mode"
                        image="https://i.postimg.cc/GtBzfZ79/Genre-Music-Hard.png"
                        url="https://anilist.co/forum/thread/6375/comment/675391"
                        small />
                </BadgeGroup>
                <BadgeGroup name="Mystery">
                    <Badge
                        name="Easy Mode"
                        image="https://i.postimg.cc/mk0VW8Jj/Genre-Mystery-Easy.png"
                        url="https://anilist.co/forum/thread/5557/comment/675394"
                        started="2021-02-20"
                        completed="2021-05-28"
                        small />
                    <Badge
                        name="Normal Mode"
                        image="https://i.postimg.cc/WpK5Z1zZ/Genre-Mystery-Normal.png"
                        url="https://anilist.co/forum/thread/5557/comment/675394"
                        started="2021-02-20"
                        completed="2022-04-09" />
                    <Badge
                        name="Hard Mode"
                        image="https://i.postimg.cc/t4GDrvhC/Genre-Mystery-Hard.png"
                        url="https://anilist.co/forum/thread/5557/comment/675394"
                        started="2021-02-20"
                        small />
                </BadgeGroup>
                <BadgeGroup name="Thriller"> {/* Page 55 */}
                    <Badge
                        name="Easy Mode"
                        image="https://i.postimg.cc/tgXkb1SK/Genre-Psych-Thriller-Easy.png"
                        url="https://anilist.co/forum/thread/5556/comment/675397"
                        started="2021-02-20"
                        completed="2021-08-01"
                        small />
                    <Badge
                        name="Normal Mode"
                        image="https://i.postimg.cc/GpqQmc6H/Genre-Psych-Thriller-Normal.png"
                        url="https://anilist.co/forum/thread/5556/comment/675397"
                        started="2021-02-20"
                        completed="2021-12-14" />
                    <Badge
                        name="Hard Mode"
                        image="https://i.postimg.cc/50ymHhMp/Genre-Psych-Thriller-Hard.png"
                        url="https://anilist.co/forum/thread/5556/comment/675397"
                        started="2021-02-20"
                        small />
                </BadgeGroup>
                <BadgeGroup name="Romance">
                    <Badge
                        name="Easy Mode"
                        image="https://i.postimg.cc/zvRS7Tvg/Genre-Romance-Easy.png"
                        url="https://anilist.co/forum/thread/5291/comment/675398"
                        started="2021-02-20"
                        completed="2021-06-26"
                        small />
                    <Badge
                        name="Normal Mode"
                        image="https://i.postimg.cc/2jqPMJB4/Genre-Romance-Normal.png"
                        url="https://anilist.co/forum/thread/5291/comment/675398"
                        started="2021-02-20"
                        completed="2022-04-02" />
                    <Badge
                        name="Hard Mode"
                        image="https://i.postimg.cc/qB99TL8W/Genre-Romance-Hard.png"
                        url="https://anilist.co/forum/thread/5291/comment/675398"
                        started="2021-02-20"
                        small />
                </BadgeGroup>
                <BadgeGroup name="Sci-Fi">
                    <Badge
                        name="Easy Mode"
                        image="https://i.postimg.cc/pXz3XFCr/Genre-Sci-Fi-Easy.png"
                        url="https://anilist.co/forum/thread/5292/comment/675400"
                        started="2021-02-20"
                        completed="2021-11-03" />
                    <Badge
                        name="Normal Mode"
                        image="https://i.postimg.cc/CKL2N6gn/Genre-Sci-Fi-Normal.png"
                        url="https://anilist.co/forum/thread/5292/comment/675400"
                        started="2021-02-20"
                        small />
                    <Badge
                        name="Hard Mode"
                        image="https://i.postimg.cc/261Kpb3z/Genre-Sci-Fi-Hard.png"
                        url="https://anilist.co/forum/thread/5292/comment/675400"
                        small />
                </BadgeGroup>
                <BadgeGroup name="Slice of Life">
                    <Badge
                        name="Easy Mode"
                        image="https://i.postimg.cc/Qtvnj8Wc/Genre-Slice-Of-Life-Easy.png"
                        url="https://anilist.co/forum/thread/5293/comment/675403"
                        started="2021-02-20"
                        completed="2021-07-05"
                        small />
                    <Badge
                        name="Normal Mode"
                        image="https://i.postimg.cc/L5fQXczw/Genre-Slice-Of-Life-Normal.png"
                        url="https://anilist.co/forum/thread/5293/comment/675403"
                        started="2021-02-20"
                        completed="2021-11-03"
                        small />
                    <Badge
                        name="Hard Mode"
                        image="https://i.postimg.cc/SQzvDMwd/Genre-Slice-Of-Life-Hard.png"
                        url="https://anilist.co/forum/thread/5293/comment/675403"
                        started="2021-02-20"
                        completed="2022-06-26" />
                    <Badge
                        name="Mastery"
                        image="https://i.postimg.cc/J0WMsdKf/AWC-Genre-Slice-Of-Life-Mastery-Animated.png"
                        url="https://anilist.co/forum/thread/5293/comment/1848342"
                        started="2022-07-04"
                        small />
                </BadgeGroup>
                <BadgeGroup name="Sports">
                    <Badge
                        name="Easy Mode"
                        image="https://i.postimg.cc/zXZPMfNr/Genre-Sports-Easy.png"
                        url="https://anilist.co/forum/thread/5876/comment/675404"
                        started="2021-02-20"
                        completed="2022-07-15" />
                    <Badge
                        name="Normal Mode"
                        image="https://i.postimg.cc/gkVBPPc6/Genre-Sports-Normal.png"
                        url="https://anilist.co/forum/thread/5876/comment/675404"
                        started="2021-02-20"
                        small />
                    <Badge
                        name="Hard Mode"
                        image="https://i.postimg.cc/cLdkx56k/Genre-Sports-Hard.png"
                        url="https://anilist.co/forum/thread/5876/comment/675404"
                        small />
                </BadgeGroup>
                <BadgeGroup name="Supernatural">
                    <Badge
                        name="Easy Mode"
                        image="https://i.postimg.cc/VN67Mrqj/Genre-Supernatural-Easy.png"
                        url="https://anilist.co/forum/thread/6630/comment/675406"
                        started="2021-02-20"
                        completed="2021-07-05"
                        small />
                    <Badge
                        name="Normal Mode"
                        image="https://i.postimg.cc/tgMBzqk8/Genre-Supernatural-Normal.png"
                        url="https://anilist.co/forum/thread/6630/comment/675406"
                        started="2021-02-20"
                        completed="2021-09-25"
                        small />
                    <Badge
                        name="Hard Mode"
                        image="https://i.postimg.cc/Y9Lbn5y0/Genre-Supernatural-Hard.png"
                        url="https://anilist.co/forum/thread/6630/comment/675406"
                        started="2021-02-20"
                        completed="2022-05-11" />
                    <Badge
                        name="Mastery"
                        image="https://i.postimg.cc/7hv0fDFq/AWC-Genre-Supernatural-Mastery-Animated.png"
                        url="https://anilist.co/forum/thread/6630/comment/1765248"
                        started="2022-05-19"
                        small />
                </BadgeGroup>
            </BadgeCategory>
        </div>

        <h1>Manga</h1>
        <div class="flex flex-wrap">
            <BadgeCategory title="Tier">
                <Badge
                    name="Beginner's"
                    image="https://i.postimg.cc/Dw0fdhy6/Tier-Manga-Beginners.png"
                    url="https://anilist.co/forum/thread/6395/comment/636272"
                    started="2021-02-04"
                    completed="2021-10-14" />
                <Badge
                    name="Intermediate"
                    image="https://i.postimg.cc/t4cqBcsj/Tier-Mange-Intermediate.png"
                    url="https://anilist.co/forum/thread/10665/comment/1342182"
                    started="2021-10-22" />
            </BadgeCategory>
            <BadgeCategory title="Manga City">
                <Badge // Release: 2019-04-19
                    name="School"
                    image="https://i.postimg.cc/zvdZVLWr/Manga-City-School.png"
                    url="https://anilist.co/forum/thread/7196/comment/644329"
                    started="2021-02-08"
                    completed="2021-11-17" />
                <Badge // Release: 2019-06-14
                    name="Museum"
                    image="https://i.postimg.cc/fTLQ1KnT/Manga-City-Museum.png"
                    url="https://anilist.co/forum/thread/7915/comment/644340"
                    started="2021-02-08" />
                <Badge // Release: 2019-08-16
                    name="Beach"
                    image="https://i.postimg.cc/K83S8886/Manga-City-Beach.png"
                    url="https://anilist.co/forum/thread/8889/comment/644352"
                    started="2021-02-08"
                    completed="2021-11-25" />
                <Badge // Release: 2019-10-18
                    name="Cemetery"
                    image="https://i.postimg.cc/HLGGc1JV/Manga-City-Cemetery.png"
                    url="https://anilist.co/forum/thread/9675/comment/644363"
                    started="2021-02-08" />
                <Badge // Release: 2020-03-07
                    name="Airport"
                    image="https://i.postimg.cc/DZFt6QPh/Manga-City-Airport.png"
                    url="https://anilist.co/forum/thread/11261/comment/644371"
                    started="2021-02-08" />
                <Badge // Release: 2020-05-16
                    name="Casino"
                    image="https://i.postimg.cc/HnkFDpNP/Manga-City-Casino.png"
                    url="https://anilist.co/forum/thread/12658/comment/644377"
                    started="2021-02-08" />
                <Badge // Release: 2020-09-25
                    name="Office"
                    image="https://i.postimg.cc/J4tSpzZs/Manga-City-Office.png"
                    url="https://anilist.co/forum/thread/17325/comment/644387"
                    started="2021-02-08"
                    completed="2021-11-20" />
                <Badge // Release: 2020-11-13
                    name="Amusement Park"
                    image="https://i.postimg.cc/TPjsKLM4/Manga-City-Amusement-Park.png"
                    url="https://anilist.co/forum/thread/19440/comment/674317"
                    started="2021-02-20"
                    completed="2021-11-25" />
                <Badge // Release: 2021-03-05
                    name="Garage"
                    image="https://i.postimg.cc/HnNZqbXV/Manga-City-Garage.png"
                    url="https://anilist.co/forum/thread/26466/comment/709828"
                    started="2021-03-05" />
                <Badge // Release: 2021-05-08
                    name="Konbini"
                    image="https://i.postimg.cc/VL3Z4Bzh/Manga-City-Konbini.png"
                    url="https://anilist.co/forum/thread/32949/comment/920539"
                    started="2021-05-09"
                    completed="2021-11-17" />
                <Badge // Release: 2021-09-10
                    name="Onsen"
                    image="https://i.postimg.cc/7LCrTGbS/Manga-City-Onsen.png"
                    url="https://anilist.co/forum/thread/42357/comment/1242745"
                    started="2021-09-11"
                    completed="2022-03-11" />
                <Badge // Release: 2021-11-26
                    name="Military Base"
                    image="https://i.postimg.cc/BZHsW-9vb/Manga-City-Military-Base.png"
                    url="https://anilist.co/forum/thread/47135/comment/1407009"
                    started="2021-11-26" />
                <Badge // Release: 2022-01-21
                    name="Hospital"
                    image="https://i.postimg.cc/0N8pTH9K/Manga-City-Hospital.png"
                    url="https://anilist.co/forum/thread/49909/comment/1515647"
                    started="2022-01-21" />
                <Badge // Release: 2022-03-25
                    name="Park"
                    image="https://i.postimg.cc/3NztXtzh/Manga-City-Park.png"
                    url="https://anilist.co/forum/thread/52601/comment/1648722"
                    started="2022-03-25" />
                <Badge
                    name="Home"
                    image="https://i.postimg.cc/J42PS8NS/Manga-City-Home.png"
                    url="https://anilist.co/forum/thread/55870/comment/1806138"
                    started="2022-06-11" />
                <Badge
                    name="Construction Site"
                    image="https://i.postimg.cc/8cb42cNc/Manga-City-Construction-Site.png"
                    url="https://anilist.co/forum/thread/60670/comment/2042836"
                    started="2022-11-11" />
                <Badge
                    name="Mall"
                    image="https://i.postimg.cc/NF1mNPkb/Manga-City-Mall.png"
                    url="https://anilist.co/forum/thread/63781/comment/2207404"
                    started="2023-03-17" />
            </BadgeCategory>
            <BadgeCategory title="Special">
                <BadgeGroup name="Explorer"> {/* Release: 2019-11-08 */}
                    <Badge
                        name="Field Path"
                        image="https://i.postimg.cc/9Q8CgnNp/Special-Explorers-Field.png"
                        url="https://anilist.co/forum/thread/9960/comment/636520"
                        started="2021-02-04"
                        completed="2021-10-07" />
                    <Badge
                        name="Mountain Path"
                        image="https://i.postimg.cc/HksT2g3n/Special-Explorers-Mountain.png"
                        url="https://anilist.co/forum/thread/9960/comment/636520"
                        started="2021-02-04"
                        completed="2022-07-17" />
                    <Badge
                        name="Ocean Path"
                        image="https://i.postimg.cc/5tX1hShM/Special-Explorers-Ocean.png"
                        url="https://anilist.co/forum/thread/9960/comment/636520"
                        started="2021-02-04" />
                </BadgeGroup>
                <Badge // Release: 2020-07-10
                    name="Recommendations"
                    image="https://i.postimg.cc/rFCcSb9s/Special-Recommendations.png"
                    url="https://anilist.co/forum/thread/14236/comment/741395"
                    started="2021-03-16" />
                <BadgeGroup name="Space Adventurer"> {/* Release: 2020-08-21 */}
                    <Badge
                        name="Voyager Path"
                        image="https://i.postimg.cc/cJPZ08Wt/Special-Space-Adventurers-Voyager.png"
                        url="https://anilist.co/forum/thread/16045/comment/674315"
                        started="2021-02-20"
                        completed="2022-03-11" />
                    <Badge
                        name="Invader Path"
                        image="https://i.postimg.cc/rF98xm17/Special-Space-Adventurers-Invader.png"
                        url="https://anilist.co/forum/thread/16045/comment/674315"
                        started="2022-03-15" />
                </BadgeGroup>
                <Badge // Release: 2021-01-20
                    name="Manga Lottery"
                    image="https://i.postimg.cc/gJRpJkcS/Special-Manga-Lottery.png"
                    url="https://anilist.co/forum/thread/23111/comment/635867"
                    started="2021-02-04"
                    completed="2021-10-14" />
                <Badge // Release: 2021-07-09
                    name="Art & Artists"
                    image="https://i.postimg.cc/MKssGkLQ/Special-Art-Artists.png"
                    url="https://anilist.co/forum/thread/38004/comment/1088432"
                    started="2021-07-09" />
                <Badge // Release: 2021-11-12
                    name="Formula 1"
                    image="https://i.postimg.cc/SNhLPfYc/Special-Formula1-Temp.png"
                    url="https://anilist.co/forum/thread/46422/comment/1380889"
                    started="2021-11-12" />
                <Badge // Release: 2022-12-16
                    name="Light Novel"
                    image="https://i.postimg.cc/kgyjdqfj/Special-Light-Novel.png"
                    url="https://anilist.co/forum/thread/61414/comment/2074426"
                    started="2022-12-16" />
            </BadgeCategory>
            <BadgeCategory title="Collection">
                <Badge // Release: 2023-01-20
                    name="Naoki Urasawa"
                    image="https://i.postimg.cc/zGYdjqsK/Collection-Naoki-Urasawa.png"
                    url="https://anilist.co/forum/thread/62321/comment/2137541"
                    started="2023-01-20" />
            </BadgeCategory>
        </div>

        <div class="my-10">
            <a href="https://gitlab.com/Lichthagel/awcbadges" target="_blank" rel="noreferrer">gitlab</a>&nbsp;
            <Link href="/cct">cct</Link>
        </div>
    </div>;


export default App;
