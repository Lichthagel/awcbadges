import clsx from "clsx";
import { Component, createSignal } from "solid-js";

const Badge: Component<{
    name: string;
    image?: string;
    url: string;
    started?: string;
    completed?: string;
    status?: string;
    small?: boolean;
}> = ({ name, image, url, started, completed, status, small }) => {

    const [loaded, setLoaded] = createSignal(false);

    return (
        <a href={url} target="_blank" rel="noreferrer">
            <div class={clsx("group inline-block relative mx-1 mb-7 transition align-bottom hover:-translate-y-2 hover:z-10", {
                "invisible opacity-0": !loaded
            })}>
                <img src={image || "https://awc.moe/static/images/badge-placeholder.png" }
                    alt={name}
                    loading="lazy"
                    class={clsx("transition group-hover:saturate-100 group-hover:opacity-100", {
                        "opacity-50 saturate-0": !completed,
                        "h-36": small,
                        "h-48": !small
                    })}
                    onLoad={() => {
                        setLoaded(true);
                    }} />
                {status && <div class="uppercase absolute -bottom-3 left-1/2 -ml-14 w-28 font-bold rounded text-xs py-2 flex justify-center bg-ctp-lavender shadow shadow-ctp-lavender text-ctp-base">{status}</div>}
                {!status && !completed && started &&
                    <div class="uppercase absolute -bottom-3 left-1/2 -ml-14 w-28 font-bold rounded text-xs py-2 flex justify-center bg-ctp-green shadow shadow-ctp-green text-ctp-base">in progress</div>
                }
                <div class={clsx("invisible opacity-0 absolute top-[105%] left-1/2 -ml-20 w-40 bg-ctp-surface0/80 rounded-md shadow shadow-ctp-surface0 transition group-hover:visible group-hover:opacity-100")}>
                    <b>{name}</b>
                    {started && <div>▶ {started}</div>}
                    {completed && <div>🏁 {completed}</div>}
                </div>
            </div>
        </a>
    );
};

export default Badge;