import { Component, createMemo, JSX, children as resolveChildren } from "solid-js";

const BadgeGroup: Component<{ name: string; revert?: boolean; children: JSX.Element }> = ({ name, children, revert }) => {
    const resolved = resolveChildren(() => children);

    const resolvedChildren = createMemo(() => {
        let list = resolved();
        if (!Array.isArray(list)) list = [list];
        if (revert) {
            return list.reverse();
        } else {
            return list;
        }
    });

    return <div class="inline-block bg-ctp-mantle rounded m-1 p-2 shadow">
        {resolvedChildren()}
        <h4 class="uppercase font-light ml-auto mr-1 w-max">{name}</h4>
    </div>;
};

export default BadgeGroup;