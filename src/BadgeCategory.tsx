import { Component, JSX, children as resolveChildren, createMemo } from "solid-js";

const BadgeCategory: Component<{ title: string; revert?: boolean; children: JSX.Element }> = ({ title, children, revert }) => {
    const resolved = resolveChildren(() => children);

    const resolvedChildren = createMemo(() => {
        let list = resolved();
        if (!Array.isArray(list)) list = [list];
        if (revert) {
            return list.reverse();
        } else {
            return list;
        }
    });

    return <div class="inline-block flex-grow flex-shrink basis-auto">
        <h2>{title}</h2>
        {resolvedChildren()}
    </div>;
};

export default BadgeCategory;