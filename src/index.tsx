import App from "./App.jsx";
import "@fontsource/readex-pro/variable.css";
import "./index.css";
import { render } from "solid-js/web";
import { Router } from "@solidjs/router";

// eslint-disable-next-line @typescript-eslint/no-non-null-assertion
render(
    () => <Router>
        <App />
    </Router>
    , document.getElementById("root") as HTMLElement
);
