import { Route, Routes } from "@solidjs/router";
import { Component, lazy } from "solid-js";

const BadgePage = lazy(() => import("./BadgePage"));
const CCT = lazy(() => import("./CCT"));

const App: Component = () =>
    <Routes>
        <Route path="/" component={BadgePage} />
        <Route path="/cct" component={CCT} />
    </Routes>;
export default App;