/** @type {import('tailwindcss').Config} */
const defaultTheme = require('tailwindcss/defaultTheme');

module.exports = {
  content: [
    "./index.html",
    "./src/**/*.{js,ts,jsx,tsx}"
  ],
  theme: {
    extend: {},
    fontFamily: {
      sans: ['Readex ProVariable', 'Readex Pro', ...defaultTheme.fontFamily.sans]
    }
  },
  plugins: [
    require("tailwind-scrollbar"),
    require("@catppuccin/tailwindcss")({
      prefix: "ctp"
    })
  ],
}
