# [AWCBadges](badges.lichthagel.de)

[![pipeline status](https://gitlab.com/Lichthagel/awcbadges/badges/master/pipeline.svg)](https://gitlab.com/Lichthagel/awcbadges/commits/master)

A simple web page for displaying my AWC challenge badges (for more info see [here](https://anilist.co/forum/thread/4449)).

## Get your own

- Fork this repo
- Adjust `src/badges.json`
- Your page will be available at `username.gitlab.io/awcbadges` (see [GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/))

## badges.json

```
{
    "anime": {
        "monthly": [...badges...],
        "seasonal": [...badges...],
        "yearly": [...badges...],
        "tier": [..badges...],
        "special": [...badges...],
        "franchise": [...badges...],
        "studio": [...badges...],
        "legend": [...badges...],
        "puzzle": [...badges...],
        "genre": [...badges...]
    },
    "manga": {
        "tier": [...badges...],
        "manga_city": [..badges...],
        "special": [...badges...]
    }
}
```

Categories can be adjusted in `src/templates/badges.hbs`

```
{
    "name": "...", // Challenge Name
    "image": "https://awc.nymh.moe/assets/xxxxxx/xxxxx.png", // Badge Image Link
    "url": "https://anilist.co/forum/thread/00000/comment/00000", // Challenge Post Link
    "started": "YYYY-MM-DD", // Start Date
    "completed": "YYYY-MM-DD", // optional, Finish Date, if unset displays b/w-badge and green "in progress"-ribbon
    // alernatively
    "completed": {
        "Easy": "YYYY-MM-DD",
        "Medium": "YYYY-MM-DD",
        "Hard": "YYYY-MM-DD"
    },
    "status": "..." // optional, displays a blue ribbon with given text
}
```

## Used stuff

- [Node](https://nodejs.org/en/)
- [Nunito](https://fonts.google.com/specimen/Nunito)
- [gulp](https://gulpjs.com/)
- [gulp-clean](https://www.npmjs.com/package/gulp-clean)
- [gulp-clean-css](https://www.npmjs.com/package/gulp-clean-css)
- [gulp-htmlmin](https://www.npmjs.com/package/gulp-htmlmin)
- [gulp-rename](https://www.npmjs.com/package/gulp-rename)
- [gulp-sitemap](https://www.npmjs.com/package/gulp-sitemap)
- [merge-stream](https://www.npmjs.com/package/merge-stream)
- [gulp-hb](https://github.com/shannonmoeller/gulp-hb)
- [handlebars](https://handlebarsjs.com/)
- [gulp-sass](https://github.com/dlmanning/gulp-sass)